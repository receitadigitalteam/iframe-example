import React from 'react';
import './App.css';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { ProgressBar } from 'primereact/progressbar';
import md5 from 'md5';

const API_KEY = "CHF7H6IFFEDA93843G89JB4A523E7I59";
const API_SECRET = "AG10F9A2B5FJA0IIFBI91EAE0F7403IIFA1261J4";
//const API_URL = "http://localhost:4200/recipe/new/";
const API_URL = "https://testing.receitadigital.com/recipe/new/";


class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      patients: [
        { name: "Rubens dos Santos", cpf: '03649005638' },
        { name: "João Laércio Villela Areias", cpf: '98315773704' },
        { name: "Edson Bernardes", cpf: '09850759615' }

      ],
      cpfPrescriber: '70796774405',
      displayModal: false,
      urlScreenPrescription: '',
      loaded: true
    };

    this.createPrescriptionTemplate = this.createPrescriptionTemplate.bind(this);
    this.isVisibleModal = this.isVisibleModal.bind(this);
    this.loaded = this.loaded.bind(this);
  }

  createPrescriptionTemplate(row) {
    return <Button label="Criar prescrição" onClick={() => this.openModalPrescription(row)} />;
  }

  isVisibleModal(isVisible){
    this.setState({...this.state, displayModal : isVisible, loaded: true});
  }

  openModalPrescription(patient){

    const objJsonStr = JSON.stringify({patient, prescriber: {cpf: this.state.cpfPrescriber}});
    const payload = Buffer.from(objJsonStr).toString("base64");
    const TOKEN = md5(`${API_SECRET}${md5(payload)}`);
    // const urlScreenPrescription = API_URL + API_KEY + "-" + TOKEN + "?payload=" + payload;
    const urlScreenPrescription = 'https://homolog.receitadigital.com/recipe/new/H9HG31F481JI64HAAE84IF6A6DE968ID-871fdf1807664cc665c6523efc71c70a?payload=eyJwYXRpZW50Ijp7ImNwZiI6IjExMjQ2ODAzNzM5In0sInByZXNjcmliZXIiOnsiY3BmIjoiMTUxMTkwMTY3NTgifX0%3D';
    console.log(urlScreenPrescription);

    this.setState({...this.state, urlScreenPrescription: urlScreenPrescription, displayModal: true});
  }

  loaded(){
    this.setState({...this.state, loaded: false});
    console.log("loaded");
  }

  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <div className="card">
          <DataTable value={this.state.patients}>
            <Column field="name" header="Name"></Column>
            <Column field="cpf" header="CPF"></Column>
            <Column header="Prescrição" body={this.createPrescriptionTemplate}></Column>
          </DataTable>
        </div>
        <Dialog header="Header" visible={this.state.displayModal} style={{ width: '80vw', height: '100vw' }} maximizable modal onHide={() => this.isVisibleModal(false)}>
          <div>

          {this.state.loaded && <ProgressBar mode="indeterminate" />}
          <iframe src={this.state.urlScreenPrescription} width="100%" height="720px" title="Prescrições"  onLoad={this.loaded}></iframe>
          </div>

        </Dialog>
      </div>
    );
  }
}
export default App;
